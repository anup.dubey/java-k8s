#FROM maven:3.6.1-jdk-11 AS build
#ARG GIT_APP_REPO
#WORKDIR /root/$GIT_APP_REPO
#ADD . /root/$GIT_APP_REPO
#RUN mvn clean install
FROM openjdk:11.0.3-jdk-slim
COPY target/*.jar /var/www/app.jar
#COPY --from=build /root/$GIT_APP_REPO/target/$GIT_APP_REPO.jar /var/$GIT_APP_REPO/$GIT_APP_REPO.jar
EXPOSE 8080
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar /var/www/app.jar" ]
